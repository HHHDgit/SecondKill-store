# SecondKill-store

#### 介绍
软件秒杀系统——根据b站学习后，自己开发了一遍，能抗住百万计压力测试

#### 软件架构
软件架构说明
* SSM环境搭建
* 集成Thymeleaf，RespBean
* MybatisPlus
* RabbitMQ
* Redis

#### 功能列表

1. 用户登录
    - 明文密码二次MD5加密
    - 参数校验+全局异常处理
    - 共享Session(二选一，我选的是Redis)
        - SpringSession
        - Redis
2. 商品列表
    - 页面缓存+URL缓存+对象缓存
    - 页面静态化，前后端分离
3. 秒杀
    - Redis预减库存减少数据库访问
    - 内存标记减少Redis的访问
    - RabbitMQ异步下单，负载均衡，削峰填谷
        - SpringBoot整合RabbitMQ
        - 交换机
4. 订单详情
    - 页面缓存+URL缓存+对象缓存
    - 页面静态化，前后端分离

#### 参与贡献
## 菜鸟项目，请大家多担待
1.  Fork 本仓库
2.  新建 Master 分支
3.  提交代码
4.  新建 Pull Request

